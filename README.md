# README #
The coding challenge was to demonstrate knowledge of java-script in integrating different social Media API through a login page.


### Overview###

Demonstrating Social Media Authentication by Logging in through a.html (which launches a pop-up for authentication via a social medium platform) and then transition to b.html, which transitions back to a.html on logging out.  
b.hmtl uses different gigya social media API for getting user information and sharing UI experiences. 
* 1.0.0


### Setup Instructions ###

- Have a webserver running
- Clone this repository into the web apps folder
- Launch a.html via your web browser using http://localhost:[port-number]/a.html
- Login with your favourite Social Media Provider
- Share an Experience
- Log out