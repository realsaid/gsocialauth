/**
 * ScriptName:publishuseraction.js
 * Create and Publish User Action
 */
function showShareUI(operationMode) {
	var act = new gigya.socialize.UserAction();
	act.setTitle("Song Pick of the Day: Adele, When we were young");  // Setting the Title
	act.setSubtitle("Adele, When we Were Young");
	act.setDescription("You look like a movie, You sound like a song.My gad, this reminds me Of when we were young");   // Setting Description
	act.setLinkBack("https://www.youtube.com/watch?v=DDWKuo3gXMQ");  // Setting the Link Back
	act.addActionLink("Adele", "http://adele.com/");  // Adding Action Link
	 
	// Adding an image that will be presented in the preview on the Share UI
	act.addMediaItem( { type: 'image', src: 'http://img.youtube.com/vi/DDWKuo3gXMQ/mqdefault.jpg', href: 'https://www.youtube.com/watch?v=DDWKuo3gXMQ' });
	 
	 var params =
            {
                userAction: act  // The UserAction object enfolding the newsfeed data. 
                ,operationMode: operationMode// Opens the Share Plugin either in Simple or Multiselect mode according to the user connection status.
                ,snapToElementID: "btnShare" // Snaps the Simple Share Plugin to the share button
                ,onError: onError  // onError method will be summoned if an error occurs.
                ,onSendDone: onSendDone // onError method will be summoned after
                                    // Gigya finishes the publishing process.
                ,context: operationMode        
                ,showMoreButton: true // Enable the "More" button and screen
                ,showEmailButton: true // Enable the "Email" button and screen
            };
	 
	// Show the "Share" dialog
	gigya.socialize.showShareUI(params);
}

//In case of an error trigger this function		
function onError(event) {
			alert('An error has occured' + ': ' + event.errorCode + '; ' + event.errorMessage);
	}

//Once the news field is published this event handler is triggered to display some fields 
function onSendDone(event) {
            document.getElementById('status').style.color = "green";
            switch(event.context)
            {
                case 'multiSelect':
                    document.getElementById('status').innerHTML = 'The newsfeed has been posted to: '  + event.providers;
                    break;
                case 'simpleShare':
                    document.getElementById('status').innerHTML = 'Clicked '  + event.providers;
                    break;
                default:
                    document.getElementById('status').innerHTML = 'Share onSendDone' ;
            }
}

