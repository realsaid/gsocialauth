/**
 * ScriptName:utilities.js
 * Utility library for commonly used functions
 * SetCookie, GetCookie, getLoginCounter, incrLoginCounter, getLastLoginDate,isEmpty
 */
 
 //Sets Cookie with a particular value and a given expiry duration
function setCookie(cookieName,cookieValue,expiryDuration) {
					var d = new Date();
					d.setTime(d.getTime() + (expiryDuration*24*60*60*1000));
					var expires = "expires=" + d.toGMTString();
					document.cookie = cookieName+"="+cookieValue+"; "+expires;
}

//Retrieves a cookie's given a cookie name
function getCookie(cookieName) {
			var name = cookieName + "=";
			var ca = document.cookie.split(';');
			for(var i=0; i<ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0)==' ') {
						c = c.substring(1);
				}
				if (c.indexOf(name) == 0) {
					return c.substring(name.length, c.length);
					}
				}
				return "";
}

//get the number of user login counts
//4)a.html Bonus- points: show some kind of counter or flag indicating how
//many times the user has logged in (e.g., use a cookie or database).
function getLoginCounter(){
		if(isEmpty(getCookie("loginCounter"))){
				setCookie("loginCounter", "0", 365);
				}
		return getCookie("loginCounter");
}

//Increaments the user login counts
function incrLoginCounter(){
		var currentCount = Number(getLoginCounter())+1;
		setCookie("loginCounter", currentCount.toString(),365);
}

//Converts a given timestamp or date to it's UTC String equivalent
function strTimeStamp(timestamp){
	return new Date(timestamp).toUTCString();
}

//Sets the Last Login date 
function setLastLoginDate(strDate){
	 setCookie("lastLoginDate",strTimeStamp(strDate), 365);
}

//Gets the last login Date
function getLastLoginDate(){
	 return getCookie("lastLoginDate");
}

//Checks if a given object or String is empty
function isEmpty(o){
	return (o === null) || (o === "");
}
